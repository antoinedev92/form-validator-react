import React, {useState, useEffect} from 'react'
//on importe la fonction pour checker les champs de formulaire.
import {validateInputField} from '../helpers/form-validator'

import axios from 'axios'

const Form = (props) =>{
    //on crée des states pour valider tous les types de champs de formulaire + une state pour les error et une pour success
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [age, setAge] = useState("")
    const [address, setAddress] = useState("")
    const [error, setError] = useState(null)
    const [success, setSuccess] = useState(null)


    //au click, on vérife les données envoyé du formulaire
    const handleSubmit = (e)=>{
        e.preventDefault()
        setError(null)
        setSuccess(null)
        /*appel de la fonction de validation de formulaire pour chacun des champs.
        on doit stocker les réponse (true ou la phrase d'erreur) dans une variable pour chacun.*/
        let emailErr = validateInputField("Email", "email", email)
        if(emailErr !== true){
            //l'erreur doit s'afficher dans le jsx (maj state error)
            setError(emailErr)
            return;
        }
        let errPass = validateInputField("Password", "password", password)
        if(errPass !== true){
            //l'erreur doit s'afficher dans le jsx (maj state error)
            setError(errPass)
            return;
        }
        let errAge = validateInputField("Age", "age", age)
        if(errAge !== true){
            //l'erreur doit s'afficher dans le jsx (maj state error)
            setError(errAge)
            return;
        }
        let errAddress = validateInputField("Adresse", "address", address)
        if(errAddress !== true){
            //l'erreur doit s'afficher dans le jsx (maj state error)
            setError(errAddress)
            return;
        }
        
        
        setSuccess("Bravo Bryan tu as réussis!")
        //const user va devenir req.body.user
        const user = {
            email: email,
            password: password,
            age: age,
            address: address
        }
        /*axios.post('http://fsjs26.ide.3wa.io:9000/postForm', user)
        .then((res)=>{
            console.log(res.data)
        })
        .catch(err=>console.log(err))*/
    }

    return (
        <div>
            <h1>Mon super formulaire du seigneur!</h1>

            {error !== null && <p style={{color: "red"}}>{error}</p>}
            {success !== null && <p style={{color: "green"}}>{success}</p>}
            
            <form
                onSubmit={handleSubmit}
            >
                <input
                    type="text"
                    placeholder="email"
                    onChange={(e)=>{
                        setEmail(e.currentTarget.value)
                    }}
                />
                <input
                    type="password"
                    placeholder="password"
                    onChange={(e)=>{
                        setPassword(e.currentTarget.value)
                    }}
                />
                <input
                    type="text"
                    placeholder="age"
                    onChange={(e)=>{
                        setAge(e.currentTarget.value)
                    }}
                />
                <input
                    type="text"
                    placeholder="adresse"
                    onChange={(e)=>{
                        setAddress(e.currentTarget.value)
                    }}
                />
                <button>Envoyer</button>
            </form>
        </div>
    )

}

export default Form