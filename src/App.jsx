import './App.css';

import Header from './components/header'
import Home from './components/home'
import Form from './components/form'

import {Routes, Route} from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Header/>
      <Routes>
        <Route exact path='/' element={<Home />}/>
        <Route exact path='/form' element={<Form />}/>
      </Routes>
    </div>
  );
}

export default App;
